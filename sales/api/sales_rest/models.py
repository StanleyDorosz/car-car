from django.db import models


class Salesperson(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.first_name} {self.last_name}, {self.employee_id}"


class Customer(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=300)
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.first_name} {self.last_name}, {self.phone_number}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField()

    def __str__(self):
        return f"{self.vin}, {self.sold}"


class Sale(models.Model):
    price = models.CharField(null=True, max_length=20)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.price}, {self.automobile}, {self.salesperson}, {self.customer}"
