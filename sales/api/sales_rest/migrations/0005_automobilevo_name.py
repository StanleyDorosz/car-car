# Generated by Django 4.0.3 on 2024-03-19 19:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("sales_rest", "0004_alter_sale_price"),
    ]

    operations = [
        migrations.AddField(
            model_name="automobilevo",
            name="name",
            field=models.CharField(max_length=200, null=True),
        ),
    ]
