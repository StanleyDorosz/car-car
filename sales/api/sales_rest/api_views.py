from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .encoders import SalespeopleListEncoder, CustomersListEncoder, SalesListEncoder
import json
from .models import AutomobileVO, Sale, Salesperson, Customer
import requests


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespeopleListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespeopleListEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request, id):

    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(salesperson, encoder=SalespeopleListEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)

        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespeopleListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomersListEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomersListEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, id):

    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(customer, encoder=CustomersListEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)

        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomersListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SalesListEncoder, safe=True)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content.get("automobile"))
            salesperson = Salesperson.objects.get(id=content.get("salesperson"))
            customer = Customer.objects.get(id=content.get("customer"))

            if automobile.sold:
                return JsonResponse({"message": "Automobile already sold"}, status=400)

            r = requests.put(
                f"http://project-beta-inventory-api-1:8000/api/automobiles/{automobile.vin}/",
                data=json.dumps({"sold": True}),
                headers={"Content-Type": "application/json"},
            )

            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=content["price"],
            )
            return JsonResponse(
                {"sale": sale},
                encoder=SalesListEncoder,
            )

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile VIN or already sold"}, status=400)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Invalid salesperson ID"}, status=400)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer ID"}, status=400)
        except KeyError as error:
            return JsonResponse({"message": f"Missing key: {error}"}, status=400)


@require_http_methods(["DELETE", "GET"])
def api_show_sale(request, id):

    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(sale, encoder=SalesListEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
