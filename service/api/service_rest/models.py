from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)

    def __str__(self):
        return self.employee_id


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.BooleanField()

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=50)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField()
    technician = models.ForeignKey(
        Technician, related_name="appointment", on_delete=models.CASCADE
    )

    def cancel(self):
        self.status = "canceled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()
