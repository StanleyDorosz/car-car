from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_id",
        "last_name",
        "first_name",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "vip",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
