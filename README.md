# CarCar
> An application for managing and organizing your car dealership

Team:

* Stanley Dorosz - Automobile Sales
* Nathan Batten - Automobile Service

## Design

![alt text](project-beta.png)

## How to Launch

This service currently runs inside of a docker environment and can be easily spun up using docker compose commands.

### Step 1

Check if docker is installed by running `docker --version` in your terminal of choice. If this command does not succeed you need to install docker. To install docker engine follow the [install docker engine](https://docs.docker.com/engine/install/) tutorial or to install the destop app follow the [install docker desktop](https://docs.docker.com/get-docker/) directions.

### Step 2

At this point you need to navigate to the root directory of the project where this README and the docker-compose.yaml file are stored. Once you are there we need to create a volume for docker to store data. To do that run `docker volume create beta-data` in your terminal.

### Step 3

Now you need to build your docker container image. While still in the root directory of this project run `docker compose build` in your terminal.

### Step 4

At this point you're ready to spin up some containers. Run `docker compose up` in your terminal and check docker desktop to make sure that all the containers are coming up properly. If all the containers are green you are ready to begin using this project.



## Service microservice

### The Poller

There is a poller inside of service that 'polls' or sends a GET request to the inventory API's list automobiles function. With that data it checks against the `AutomobileVO` objects and create or updates the value objects as needed.

### The API

This microservice comes with a Service API that keeps track of technicians and service appointments.
It has RESTful endpoints for the following entities:
* __Technician__: an individual technician who works at the dealership.
* __Appointment__: a service appointment with details such as Customer name, vehicle status, reason for the appointment, or the assigned technician
* __AutomobileVO__: a value object representing an automobile on the inventory API


### URL Routes and RESTful API endpoints

From insomnia or your browser you can access technician or appointment endpoints at the following URLS.

| Action | Method | URL |
|--------|--------|-----|
| List Technicians | GET | http://localhost:8080/api/technicians/ |
| Create a Technician | POST | http://localhost:8080/api/technicians/ |
| Delete a Technician | DELETE | http://localhost:8080/api/technicians/:id/ |
| List Appointments | GET | http://localhost:8080/api/appointments/ |
| Create an Appointment | POST | http://localhost:8080/api/appointments/ |
| Delete an Appointment | DELETE | http://localhost:8080/api/appointments/:id/ |
| Cancel an Appointment | PUT | http://localhost:8080/api/appointments/:id/cancel/ |
| Finish an Appointment | PUT | http://localhost:8080/api/appointments/:id/finish/ |

### Sample JSON

When making a POST request to Create a Technician with the above url, this is the pattern your JSON should be:
```json
{
  "first_name": "Bob",
  "last_name": "Bobkhov",
  "employee_id": "bbobkhov"
}
```

The POST request will return JSON with the properties of the newly created Technician:
```json
{
  "technician": {
    "employee_id": "bbobkhov",
	"last_name": "Bobkhov",
	"first_name": "Bob",
	"id": 6
  }
}
```

The List Technicians GET request will return JSON representing an array of `Technician` objects:
```json
{
  "technicians": [
	{
      "employee_id": "jbobkhov",
	  "last_name": "Bobkhov",
	  "first_name": "Jimmy",
	  "id": 1
	}
  ]
}
```

When making a POST request to Create an Appointment with the above url, this is the pattern your JSON should be:
```json
{
  "date_time": "2024-03-28T12:45",
  "reason": "Oil Change",
  "vin": "1C3CC5F7453120174",
  "customer": "Jake Smith",
  "technician": 2
}
```

That POST request will return JSON representing the newly created `Appointment` object with the properties of the associated `Technician` object encoded into it:
```json
{
  "appointment": {
	"id": 11,
	"date_time": "2024-03-28T12:45",
	"reason": "Spark Plugs",
	"status": "created",
	"vin": "1C3CC5F7453120174",
	"vip": true,
	"customer": "Jake Smith",
	"technician": {
      "employee_id": "bbobkhov",
	  "last_name": "Bobkhov",
	  "first_name": "Bobby",
	  "id": 2
	}
  }
}

```

The List Appointments GET request returns JSON consisting of an object of arrays each containing an `Appointment` object:
```json
{
  "appointments": [
	{
	  "id": 7,
	  "date_time": "2024-03-28T12:45:00+00:00",
	  "reason": "Spark Plugs",
	  "status": "created",
	  "vin": "1C3CC5F7453120174",
	  "vip": true,
	  "customer": "Jake Smith",
	  "technician": {
		"employee_id": "bbobkhov",
		"last_name": "Bobkhov",
		"first_name": "Bobby",
		"id": 2
	  }
	}
  ]
}
```


## Sales microservice

Restful API's for Sales Microservice:
-


| Action | Method | URL |
|--------|--------|-----|
| List Salespeople     | GET      | http://localhost:8090/api/salespeople/     |
| Create a Salesperson | POST     | http://localhost:8090/api/salespeople/     |
| Delete a Salesperson | DELETE   | http://localhost:8080/api/salespeople/:id/ |
| List Customers       | GET      | http://localhost:8090/api/customers/       |
| Create a Customer    | POST     | http://localhost:8090/api/customers/       |
| Delete a Customer    | DELETE   | http://localhost:8080/api/customers/:id/   |
| List Sales           | GET      | http://localhost:8090/api/sales/           |
| Create a Sale        | POST     | http://localhost:8090/api/sales/           |
| Delete a Sale        | DELETE   | http://localhost:8080/api/sales/:id/       |



--------------------------------------------------------------------------------------------
Example JSON:


List Salespeople:
-

{
	"salespeople": [
		{
			"id": 1,
			"first_name": "NAME",
			"last_name": "NAME",
			"employee_id": "RET1234567"
		}
	]
}

Create a Salesperson:
-

{
	"first_name": "NAME",
	"last_name": "NAME",
	"employee_id": "GHI123456543"
}


Delete a Salesperson:
-

{
	"deleted": true
}



List Customers:
-

{
	"customers": [
		{
			"id": 1,
			"first_name": "NAME",
			"last_name": "NAME",
			"address": "EXAMPLE ADDRESS",
			"phone_number": 1234567890"
		},
	]
}


Create a Customer:
-

{
			"first_name": "NAME",
			"last_name": "NAME",
			"address": "EXAMPLE ADDRESS",
			"phone_number": "1234567890"
		}


Delete a Customer:
-

{
	"deleted": true
}

List Sales:
-

{
	"sales": [
		{
			"id": 8,
			"price": "$50,000.00",
			"automobile": {
				"id": 2,
				"vin": "5NPEC4AB2BH271832",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "NAME",
				"last_name": "NAME",
				"employee_id": "RET1234567"
			},
			"customer": {
				"id": 1,
				"first_name": "NAME",
				"last_name": "NAME",
				"address": "EXAMPLE ADDRESS",
				"phone_number": "1234567890"
			}
		}
	]
}


Create a Sale:

	{
			"price": "$50,000.00",
			"automobile": {
				"id": 2,
				"vin": "5NPEC4AB2BH271832"
			},
			"salesperson": {
				"id": 1
			},

			"customer": {
				"id": 1
			}
	}



Delete a Sale:
-

{
	"deleted": true
}



---------------------------------------------------------------------------------------------

|KEY: For Model Table|
|--------------------|
|fk = "foreign key"  |
|O2M = "One To Many" |
|VO = "Value Object" |

--------------------------------------------------------------------------------------------


| Model        | Properties                                          | Description                     |
|--------------|-----------------------------------------------------|---------------------------------|
| Salesperson  | first_name, last_name, employee_id                  |  Salespeople data structure     |
| Customer     | first_name, last_name, address, phone_number        |  Customers data structure       |
| Sale         | price, automobile(fk), salesperson(fk), customer(fk)|  Sales data structure O2M 3fk's |
| AutomobileVO | vin, sold                                           | Automobile VO data structure    |
