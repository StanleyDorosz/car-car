import React, { useEffect, useState } from "react";

function AddASaleForm() {
  const [autos, setAutos] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [formData, setFormData] = useState({
    selectedAuto: "",
    selectedSalesperson: "",
    selectedCustomer: "",
    price: "",
  });

  useEffect(() => {
    fetchAutosData();
    fetchSalespeopleData();
    fetchCustomersData();
  }, []);

  async function fetchAutosData() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }

  async function fetchSalespeopleData() {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  async function fetchCustomersData() {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      automobile: formData.selectedAuto,
      salesperson: formData.selectedSalesperson,
      customer: formData.selectedCustomer,
      price: formData.price,
    };

    const saleUrl = "http://localhost:8090/api/sales/";
    const response = await fetch(saleUrl, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (response.ok) {
      alert("Sale added successfully!");
      setFormData({
        selectedAuto: "",
        selectedSalesperson: "",
        selectedCustomer: "",
        price: "",
      });
    } else {
      const errorData = await response.json();
      alert(
        `Failed to add a sale. Error: ${
          errorData.message || "Please check your form data."
        }`
      );
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Sale</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="selectedAuto" className="form-label">
                Automobile
              </label>
              <select
                id="selectedAuto"
                name="selectedAuto"
                value={formData.selectedAuto}
                onChange={handleInputChange}
                className="form-select"
                required
              >
                <option value="">Choose an automobile</option>
                {autos.map((auto) => (
                  <option key={auto.id} value={auto.vin}>
                    {auto.vin}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="selectedSalesperson" className="form-label">
                Salesperson
              </label>
              <select
                id="selectedSalesperson"
                name="selectedSalesperson"
                value={formData.selectedSalesperson}
                onChange={handleInputChange}
                className="form-select"
                required
              >
                <option value="">Choose a salesperson</option>
                {salespeople.map((salesperson) => (
                  <option key={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name} {salesperson.last_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="selectedCustomer" className="form-label">
                Customer
              </label>
              <select
                id="selectedCustomer"
                name="selectedCustomer"
                value={formData.selectedCustomer}
                onChange={handleInputChange}
                className="form-select"
                required
              >
                <option value="">Choose a customer</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {customer.first_name} {customer.last_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="price"
                name="price"
                placeholder="Price"
                value={formData.price}
                onChange={handleInputChange}
                required
              />
              <label htmlFor="price">Price</label>
            </div>
            <button type="submit" className="btn btn-primary">
              Create Sale
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddASaleForm;
