import { useEffect, useState } from "react";

const CreateModelForm = () => {
  const [manufacturers, setManufacturers] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer: "",
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({
        ...formData,
        ["manufacturer_id"]: formData["manufacturer"],
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      alert("Model Created");
      setFormData({
        name: "",
        picture_url: "",
        manufacturer: "",
      });
    } else {
      alert(`Could not create model ${response.status}`);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error("Bad Response", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="header">Create a new Car Model</h1>
          <form onSubmit={handleFormSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input
                value={formData.name}
                onChange={handleFormChange}
                name="name"
                placeholder="Name"
                type="text"
                id="name"
                className="form-control"
                required
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.picture_url}
                onChange={handleFormChange}
                name="picture_url"
                placeholder="Picture URL"
                type="url"
                id="picture_url"
                className="form-control"
                required
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                value={formData.manufacturer}
                onChange={handleFormChange}
                name="manufacturer"
                id="manufacturer"
                className="form-select"
                required
              >
                <option defaultValue="" value="">
                  Choose a Manufacturer
                </option>
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateModelForm;
