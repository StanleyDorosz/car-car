import { useEffect, useState } from "react";

function VehicleModelsList() {
  const [models, setModels] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error("Failed to fetch models:", response.statusText);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="VehicleModelsList">
      <h2 className="form-title">Models</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td>
                {" "}
                <img className="img-fluid" src={model.picture_url} />{" "}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleModelsList;
