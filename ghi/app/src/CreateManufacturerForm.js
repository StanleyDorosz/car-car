import { useState } from "react";

const CreateManufacturerForm = () => {
  const [manufacturerName, setManufacturerName] = useState("");

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturerName(value);
  };

  const handleFormSubmit = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const data = { name: manufacturerName };
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      alert("Manufacturer Created");
      setManufacturerName("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="header">Create a new Manufacturer</h1>
          <form onSubmit={handleFormSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                value={manufacturerName}
                onChange={handleManufacturerChange}
                name="name"
                placeholder="Manufacturer"
                type="text"
                id="name"
                className="form-control"
                required
              />
              <label htmlFor="name">Manufacturer</label>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateManufacturerForm;
