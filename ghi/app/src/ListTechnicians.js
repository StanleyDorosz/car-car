import { useEffect, useState } from "react";

const ListTechnicians = () => {
  const [technicianList, setTechnicianList] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/";
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const technicians = data.technicians;
        setTechnicianList(technicians);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">Last Name</th>
            <th scope="col">First Name</th>
          </tr>
        </thead>
        <tbody>
          {technicianList.map((technician, index) => {
            return (
              <tr key={index}>
                <th scope="row">{technician.employee_id}</th>
                <td>{technician.last_name}</td>
                <td>{technician.first_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default ListTechnicians;
