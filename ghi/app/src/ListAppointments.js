import { useEffect, useState } from "react";

const ListAppointments = () => {
  const [showAlert, setShowAlert] = useState(false);

  const [appointmentsList, setAppointmentsList] = useState([]);

  const formatDate = (dateTime) => dateTime.slice(0, 10);
  const formatTime = (dateTime) => dateTime.slice(11, 16);
  const renderVIP = (vipStatus) => (vipStatus ? "Yes" : "No");

  const fetchData = async () => {
    const url = "http://localhost:8080/api/appointments/";
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const appointments = data.appointments;
        setAppointmentsList(appointments);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const removeArrayElement = (id) => {
    const appointments = appointmentsList.filter(
      (appointment) => appointment.id !== id
    );
    setAppointmentsList(appointments);
  };

  const handleCancelButtonClick = async (event, id) => {
    const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
    const fetchOptions = {
      method: "PUT",
    };
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      removeArrayElement(id);
      alert("Appointment Canceled");
    } else {
      alert("Response Unsuccessful", response.status);
    }
  };

  const handleFinishButtonClick = async (event, id) => {
    const url = `http://localhost:8080/api/appointments/${id}/finish/`;
    const fetchOptions = {
      method: "PUT",
    };
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      removeArrayElement(id);
      alert("Appointment Finished");
    } else {
      alert("Response Unsuccessful", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1 className="heading">Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">VIN</th>
            <th scope="col">VIP</th>
            <th scope="col">Customer</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointmentsList.map((appointment, index) => {
            if (
              appointment.status !== "canceled" &&
              appointment.status !== "finished"
            ) {
              return (
                <tr key={index}>
                  <th scope="row">{appointment.vin}</th>
                  <td>{renderVIP(appointment.vip)}</td>
                  <td>{appointment.customer}</td>
                  <td>{formatDate(appointment.date_time)}</td>
                  <td>{formatTime(appointment.date_time)}</td>
                  <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    <button
                      type="button"
                      className="btn-danger"
                      onClick={(event) => {
                        return handleCancelButtonClick(event, appointment.id);
                      }}
                    >
                      Cancel
                    </button>
                  </td>
                  <td>
                    <button
                      type="button"
                      className="btn-success"
                      onClick={(event) => {
                        return handleFinishButtonClick(event, appointment.id);
                      }}
                    >
                      Finish
                    </button>
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </>
  );
};

export default ListAppointments;
