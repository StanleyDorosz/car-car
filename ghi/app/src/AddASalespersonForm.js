import React, { useEffect, useState } from "react";

function AddASalespersonForm() {
  const [salespeople, setSalespeople] = useState([]);
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [employee_id, setEmployeeID] = useState("");

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeIDChange = (event) => {
    const value = event.target.value;
    setEmployeeID(value);
  };

  const fetchData = async () => {
    const url = "http://localhost:8090/api/salespeople/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.salespeople = salespeople;
    data.first_name = first_name;
    data.last_name = last_name;
    data.employee_id = employee_id;

    const newSalespersonUrl = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(newSalespersonUrl, fetchConfig);
    if (response.ok) {
      const newSalesperson = await response.json();

      setFirstName("");
      setLastName("");
      setEmployeeID("");

      alert("Salesperson added successfully!");
    } else {
      alert("Failed to add a Salesperson.");
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit} id="add-a-salesperson-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFirstNameChange}
                  value={first_name}
                  placeholder="First name"
                  required
                  type="text"
                  name="first_name"
                  id="first_name"
                  className="form-control"
                  autoComplete="off"
                />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleLastNameChange}
                  value={last_name}
                  placeholder="Last name"
                  required
                  type="text"
                  name="last_name"
                  id="last_name"
                  className="form-control"
                  autoComplete="off"
                />
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleEmployeeIDChange}
                  value={employee_id}
                  placeholder="Employee ID"
                  required
                  type="text"
                  name="employee_id"
                  id="employee_id"
                  className="form-control"
                  autoComplete="off"
                />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddASalespersonForm;
