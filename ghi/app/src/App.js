import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalespeopleList from "./SalespeopleList";
import CustomersList from "./CustomersList";
import SalesList from "./SalesList";
import AddASalespersonForm from "./AddASalespersonForm";
import AddACustomerForm from "./AddACustomerForm";
import ManufacturersList from "./ManufacturersList";
import VehicleModelsList from "./VehicleModelsList";
import AutomobilesList from "./AutomobilesList";
import ListTechnicians from "./ListTechnicians";
import ListAppointments from "./ListAppointments";
import ServiceHistory from "./ServiceHistory";
import CreateTechnicianForm from "./CreateTechnicianForm";
import CreateAppointmentForm from "./CreateAppointmentForm";
import CreateManufacturerForm from "./CreateManufacturerForm";
import CreateModelForm from "./CreateModelForm";
import CreateAutomobileForm from "./CreateAutomobileForm";
import Footer from "./Footer";
import SalespersonHistory from "./SalespersonHistory";
import AddASaleForm from "./AddASaleForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople/">
            <Route index element={<SalespeopleList />} />
            <Route path="create/" element={<AddASalespersonForm />} />
          </Route>
          <Route path="customers/">
            <Route index element={<CustomersList />} />
            <Route path="create/" element={<AddACustomerForm />} />
          </Route>
          <Route path="sales/">
            <Route index element={<SalesList />} />
            <Route path="history/" element={<SalespersonHistory />} />
            <Route path="create/" element={<AddASaleForm />} />
          </Route>
          <Route path="manufacturers/">
            <Route index element={<ManufacturersList />} />
            <Route path="create/" element={<CreateManufacturerForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<VehicleModelsList />} />
            <Route path="create/" element={<CreateModelForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobilesList />} />
            <Route path="create/" element={<CreateAutomobileForm />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<ListTechnicians />} />
            <Route path="create/" element={<CreateTechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<ListAppointments />} />
            <Route path="create/" element={<CreateAppointmentForm />} />
            <Route path="history/" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
