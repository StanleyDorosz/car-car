import { useEffect, useState } from "react";
import "./index.css";

const CreateAppointmentForm = () => {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    date_time: "",
    reason: "",
    vin: "",
    customer: "",
    technician: "",
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      setFormData({
        date_time: "",
        reason: "",
        status: "",
        vin: "",
        customer: "",
        technician: "",
      });
      alert("Appointment Created!");
    } else {
      alert("Unable to create Appointment");
      console.error("Bad response:", response.status);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const technicians = data.technicians;
      setTechnicians(technicians);
    } else {
      console.error("Bad response", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="header">Create an Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                value={formData.date_time}
                onChange={handleFormChange}
                type="datetime-local"
                name="date_time"
                placeholder=""
                id="date_time"
                className="form-control"
                required
              />
              <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.reason}
                onChange={handleFormChange}
                type="text"
                name="reason"
                placeholder="Reason"
                id="reason"
                className="form-control"
                required
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.vin}
                onChange={handleFormChange}
                type="text"
                name="vin"
                placeholder="VIN"
                id="vin"
                className="form-control"
                required
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.customer}
                onChange={handleFormChange}
                type="text"
                name="customer"
                placeholder="Customer"
                className="form-control"
                required
              />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="mb-3">
              <select
                value={formData.technician}
                onChange={handleFormChange}
                name="technician"
                id="technician"
                className="form-select"
                required
              >
                <option defaultValue="" value="">
                  Choose a Technician
                </option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.employee_id}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateAppointmentForm;
