import { useEffect, useState } from "react";

const ServiceHistory = () => {
  const [searchInput, setSearchInput] = useState("");
  const [appointmentsList, setAppointmentsList] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  const formatDate = (dateTime) => dateTime.slice(0, 10);

  const formatTime = (dateTime) => {
    return dateTime.slice(11, 16);
  };

  const renderVIP = (vipStatus) => (vipStatus ? "Yes" : "No");

  const handleSearchInputChange = (event) => {
    const value = event.target.value;
    setSearchInput(value);
  };

  const handleFormSubmission = (event) => {
    event.preventDefault();
    if (searchInput === "") {
      setFilteredAppointments(appointmentsList);
    } else {
      const searchVin = searchInput;
      const appointments = appointmentsList.filter(
        (appointment) => appointment.vin === searchVin
      );
      setFilteredAppointments(appointments);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const appointments = data.appointments;
      setAppointmentsList(appointments);
      setFilteredAppointments(appointments);
    } else {
      console.error("Bad Response", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1 className="header">Service History</h1>
      <div className="row">
        <form id="search-vin-form" onSubmit={handleFormSubmission}>
          <div className="input-group">
            <input
              onChange={handleSearchInputChange}
              value={searchInput}
              className="form-control rounded"
              placeholder="search vin"
              aria-label="Search"
            />
            <button className="btn btn-success">Search</button>
          </div>
        </form>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">VIN</th>
            <th scope="col">VIP</th>
            <th scope="col">Customer</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment, index) => {
            return (
              <tr key={index}>
                <th scope="row">{appointment.vin}</th>
                <td>{renderVIP(appointment.vip)}</td>
                <td>{appointment.customer}</td>
                <td>{formatDate(appointment.date_time)}</td>
                <td>{formatTime(appointment.date_time)}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

export default ServiceHistory;
