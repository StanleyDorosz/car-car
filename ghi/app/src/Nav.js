import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="appointments/"
              >
                Appointments
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="appointments/">
                  List Appointments
                </NavLink>
                <NavLink className="dropdown-item" to="appointments/create/">
                  Create Appointment
                </NavLink>
                <NavLink className="dropdown-item" to="appointments/history">
                  Appointment History
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="technicians/"
              >
                Technicians
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="technicians/">
                  List Technicians
                </NavLink>
                <NavLink className="dropdown-item" to="technicians/create/">
                  Create Technician
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="models/"
              >
                Models
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="models/">
                  List Models
                </NavLink>
                <NavLink className="dropdown-item" to="models/create/">
                  Create Model
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="automobiles/"
              >
                Automobiles
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="automobiles/">
                  List Automobiles
                </NavLink>
                <NavLink className="dropdown-item" to="automobiles/create/">
                  Create Automobiles
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="manufacturers/"
              >
                Manufacturers
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="manufacturers/">
                  List Manufacturers
                </NavLink>
                <NavLink className="dropdown-item" to="manufacturers/create/">
                  Create Manufacturers
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="salespeople/"
              >
                Sales People
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="salespeople/">
                  List Sales People
                </NavLink>
                <NavLink className="dropdown-item" to="salespeople/create/">
                  Create Sales Person
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="customers/"
              >
                Customers
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="customers/">
                  List Customers
                </NavLink>
                <NavLink className="dropdown-item" to="customers/create/">
                  Create Customer
                </NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                href="sales/"
              >
                Sales
              </a>
              <ul className="dropdown-menu">
                <NavLink className="dropdown-item" to="sales/">
                  List Sales
                </NavLink>
                <NavLink className="dropdown-item" to="sales/create/">
                  Create Sale
                </NavLink>
                <NavLink className="dropdown-item" to="sales/history/">
                  Sales History
                </NavLink>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
