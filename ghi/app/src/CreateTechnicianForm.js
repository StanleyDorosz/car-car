import { useState } from "react";

const CreateTechnicianForm = () => {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleFormSubmit = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      alert("Technician Created");

      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
    } else {
      alert("Unable to create Technician");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="header">Create a new Technician</h1>

          <form onSubmit={handleFormSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                value={formData.first_name}
                onChange={handleFormChange}
                name="first_name"
                placeholder="First Name"
                id="first_name"
                className="form-control"
                required
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.last_name}
                onChange={handleFormChange}
                name="last_name"
                placeholder="Last Name"
                id="last_name"
                className="form-control"
                required
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.employee_id}
                onChange={handleFormChange}
                name="employee_id"
                placeholder="Employee ID"
                id="employee_id"
                className="form-control"
                required
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateTechnicianForm;
