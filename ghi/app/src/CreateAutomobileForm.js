import { useEffect, useState } from "react";

const CreateAutomobileForm = () => {
  const [models, setModels] = useState([]);
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model: "",
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleFormSubmit = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({
        ...formData,
        ["model_id"]: formData["model"],
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      alert("Automobile Created");
      setFormData({
        color: "",
        year: "",
        vin: "",
        model: "",
      });
    } else {
      alert(`Unable to create automobile. Server error: ${response.status}`);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error("Unable to fetch models", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="header">Create an Automobile</h1>
          <form onSubmit={handleFormSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input
                value={formData.color}
                onChange={handleFormChange}
                name="color"
                placeholder="Color"
                type="text"
                id="color"
                className="form-control"
                required
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.year}
                onChange={handleFormChange}
                name="year"
                placeholder="Year"
                type="number"
                min="1750"
                max="9999"
                id="year"
                className="form-control"
                required
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.vin}
                onChange={handleFormChange}
                name="vin"
                placeholder="vin"
                type="text"
                id="vin"
                className="form-control"
                required
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <select
                value={formData.model}
                onChange={handleFormChange}
                name="model"
                id="model"
                className="form-select"
                required
              >
                <option defaultValue="" value="">
                  Choose a model
                </option>
                {models.map((model) => {
                  return (
                    <option key={model.id} value={model.id}>
                      {`${model.manufacturer.name} ${model.name}`}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateAutomobileForm;
