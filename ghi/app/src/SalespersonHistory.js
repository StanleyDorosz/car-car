import { useEffect, useState } from "react";

const SalespersonHistory = () => {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");
  const [sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);

  const fetchSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error("Failed to fetch salespeople");
    }
  };

  const fetchSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error("Failed to fetch sales");
    }
  };

  const handleSalespersonChange = (event) => {
    const theseFilteredSales = sales.filter(
      (sale) => sale.salesperson.employee_id === event.target.value
    );
    setFilteredSales(theseFilteredSales);
    setSelectedSalesperson(event.target.value);
  };

  useEffect(() => {
    fetchSales();
    fetchSalespeople();
  }, []);

  return (
    <>
      <h1 className="header">Salesperson History</h1>
      <div className="row">
        <div className="input-group mb-3">
          <select
            className="form-select"
            value={selectedSalesperson}
            onChange={handleSalespersonChange}
          >
            <option value="">Select a Salesperson</option>
            {salespeople.map((salesperson) => (
              <option
                key={salesperson.employee_id}
                value={salesperson.employee_id}
              >
                {salesperson.first_name}
              </option>
            ))}
          </select>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {filteredSales.map((sale, index) => (
            <tr key={index}>
              <th scope="row">{sale.salesperson.employee_id}</th>
              <td>
                {sale.salesperson.first_name} {sale.salesperson.last_name}
              </td>
              <td>
                {sale.customer.first_name} {sale.customer.last_name}
              </td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default SalespersonHistory;
